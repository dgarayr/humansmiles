'''Basic SetupTools-based installation script for HumanSMILES+LigandBuilder'''
from setuptools import setup,find_packages
setup(name='HumanSMILES',
      version='1.0',
      author="Diego Garay-Ruiz",
      url='gitlab.com/dgarayr/humansmiles',
      description="Humanization of SMILES with molecular-formula-inspired names",
      py_modules=['HumanSMILES'])
