'''
Diego Garay-Ruiz:
Nov 2020 - Jan 2021
Collection of functions to "humanize SMILES" representations to more intuitive names, inspired by molecular formulas
The initial goal was to easily build (via RDKit) a ligand database, going from SMILES to well-named XYZ files. This has
been pushed to a companion library (LigandBuilder.py)
'''

import re
from rdkit import Chem
from rdkit.Chem import Draw
import numpy as np
import copy
import itertools
from math import ceil

### Basic SMILES-orientated string processing
def first_level_locator(in_string,loc_item="parenthesis"):
	'''For a string with nested parenthesis, locate all first-level occurrencies and
	return the corresponding indices. DOES NOT CHECK for unterminated parentheses: oriented
	to previously curated, correct SMILES.
	Input:
	- in_string. String to be checked (must be a correct SMILES)
	- loc_item. String: find "parenthesis" or "bracket". If no valid string is provided, falls back to parenthesis.
	Output:
	- paired_list. List of tuples of integers (i0,i1), indicating start and end of every 1st-level parenthesis
	in the input string'''
	open_list,paired_list = ([],[])
	if (loc_item == "parenthesis"):
		locators = ["(",")"]
	elif (loc_item == "bracket"):
		locators = ["[","]"]
	else:
		# fallback to ()
		locators = ["(",")"]
		
	# flag to handle nesting
	first_par = True
	for ii,item in enumerate(in_string):
		if (item == locators[0]):
			open_list.append(ii)
			# store 1st level of nesting, then turn off the flag
			if (first_par):
				startpt = ii
			first_par = False
		elif (item == locators[1]):
			# assume it matches (we should have proper strings)
			match = open_list[-1]
			pair = (match,ii)
			open_list.remove(match)
			# discard all nesting cases, and only store matches for the 1st level of nesting
			if (startpt in pair):
				paired_list.append(pair)
				# turn the flag on again in case there are more substituents
				first_par = True
	return paired_list

def string_remover(in_string,index_list,give_frags=False):
	'''Remove fragments of a string, specified by tuples of indices. 
	Input:
	- in_string. String to be processed.
	- index_list. List of tuples of integers (i0,i1) with the slices to be removed.
	- give_frags. Boolean: if true return not only the trimmed string but also all the fragments
	that have been removed.
	Output:
	- out_string. String, with removed groups.
	- fraglist. (OPTIONAL) List of the fragments that have been removed.'''
	mutate_string = in_string
	fraglist = []
	for ndx_tuple in index_list:
		a,b = ndx_tuple
		# to keep indices, work with a placeholder string removed at the end
		placeholder = "X"*((b-a)+1)
		frag = mutate_string[a:b+1]
		# save frags WITHOUT leading pars
		fraglist.append(frag[1:-1])
		# only one match at a time
		mutate_string = re.sub(re.escape(frag),placeholder,mutate_string,count=1)
	out_string = re.sub(r"(X)\1{2,}","",mutate_string)
	if (give_frags):
		return out_string,fraglist
	else:
		return out_string

def re_smiler(smile_list):
	'''Re-format a batch of SMILES strings, changing [R] by *,
	reading the molecule in RDKit and generating output with explicit 
	hydrogens.
	Input:
	- smile_list. List of strings: SMILES defining a set of molecules
	Output:
	- output_list. List of strings: SMILES with explicit hydrogens and asterisks marking 
	substitution positions (for consistency)'''
	mol_selection = []
	output_list = []
	for ii,xx in enumerate(smile_list):
		xx2 = re.sub("\[R\]","*",xx)
		mol = Chem.MolFromSmiles(xx2)
		mol_selection.append(mol)
		outsmile = Chem.MolToSmiles(mol,allBondsExplicit=False,allHsExplicit=True)
		output_list.append(outsmile)
	return output_list

# Dictionary for monodentate substituents
common_people = {
	"[C]([F])([F])[F]":"[CF3]",
	"[F][C]([F])([F])":"[CF3]",
	"[C]([Cl])([Cl])[Cl]":"[CCl3]",
	"[Cl][C]([Cl])([Cl])":"[CCl3]",
	"[c]1[cH][cH][cH][cH][cH]1":"[Ph]",
	"[O][CH3]":"[OCH3]",
	"[C]([CH3])([CH3])[CH3]":"[tBu]",
	"[CH3][C]([CH3])([CH3])":"[tBu]",
	"[C]#[N]":"[CN]",
	"[N]#[C]":"[NC]",
	"[N+](=[O])[O-]":"[NO2]",
	"[O]=[N+]([O-])":"[NO2]",
	"[C](=[O])[OH]": "[COOH]",
	"[C]([NH2])=[O]": "[CONH2]",
	"[S](=[O])(=[O])": "[SO2]"
	}

def common_simplifier(smile):
	'''Basic substitution function for common monodentate substituents with convoluted SMILES.
	Uses manually-curated common_people dictionary inside the module.
	Input:
	- smile. String, SMILES with explicit hydrogens
	Output:
	- nw_smile. String, simplified, non-standard SMILES-like representation after
	initial substitution'''
	# dictionary to simplify common and simple substituents with convoluted SMILES
	nw_smile = smile
	for k,v in common_people.items():
		if (k in nw_smile):
			# escape brackets
			nw_smile = re.sub(re.escape(k),v,nw_smile)
	
	# process uncaught carbonyls (very common motif)
	carbonyl = "[C](=[O])"
	if (carbonyl in nw_smile):
		nw_smile = re.sub(re.escape(carbonyl),"[C=O]-",nw_smile)
	return nw_smile

def cycle_finder(smile,cyc_num=1):
	'''Detection of single-cycle patterns, via ]1 regex,
	for further processing.
	Input:
	- smile. String, expanded SMILES representation (pre-simplified!) for a given ligand
	- cyc_num. Integer, no. of cycles to handle. By now only 1 is supported.
	Output:
	- ndx. List of lists of integers of the form [i0,i1], delimiting the part of the string
	corresponding to the cycle'''
	string_fetch = "\]%d" % cyc_num
	# fetch all ]1 occurrencies that mark cycles
	match = re.finditer(string_fetch,smile)
	# for every match, we need to be sure to have the whole group previous to the number
	ndx = []
	for m in match:
		i1 = m.end() # no problem here 
		# get the start of the match and process to have the whole group
		trimstring = smile[:m.start()+1]
		# get [ and select the last one (cycle-closing)
		x = list(re.finditer("\[",trimstring))[-1] # last element only!
		i0 = x.start()
		ndx.append([i0,i1])
	return ndx

def polycycle_finder(smile,cyc_num=1):
	'''Detection of single-cycle patterns, via ]1 regex,
	for further processing.
	Input:
	- smile. String, expanded SMILES representation (pre-simplified!) for a given ligand
	- cyc_num. Integer, no. of cycles to handle. By now only 1 is supported.
	Output:
	- ndx. List of lists of integers of the form [i0,i1], delimiting the part of the string
	corresponding to the cycle'''
	string_fetch = "][0-9]+"
	# fetch all ]1 occurrencies that mark cycles
	match = re.finditer(string_fetch,smile)
	# for every match, we need to be sure to have the whole group previous to the number
	ndx = []
	for m in match:
		i1 = m.end() # no problem here 
		# get the start of the match and process to have the whole group
		trimstring = smile[:m.start()+1]
		# get [ and select the last one (cycle-closing)
		x = list(re.finditer("\[",trimstring))[-1] # last element only!
		i0 = x.start()
		ndx.append([i0,i1])
	return ndx	


def benz_smile_gen(nsubst):
	'''Automate the generation of all substitution patterns in aromatic rings
	Input:
	- nsubst. Integer. Number of substituents in the benzene.
	Output:
	- benz_dict. Dictionary mapping simplified SMILES to human-readable names, for the
	requested substituted benzene'''
	# generate combinations
	string_vals = [str(x) for x in range(2,7)]
	combx = list(itertools.combinations(string_vals,r=nsubst-1))
	# and these are already sorted! Add the 1st item, always 1
	stringlist = ["1"+"".join(items) for items in combx]
	# build namestring depending on substs
	benz_name = "C6H%d" % (6-nsubst)
	# these are the possible codes: build SMILES
	benz_dict = {}
	for entry in stringlist:
		name = entry + benz_name
		name = benz_name
		values = [int(ii) for ii in entry]
		cyc_smile_list = ["[c]" if x in values else "[cH]" for x in range(1,7)]
		# mark start and end
		cyc_smile_list[0] += "1"
		cyc_smile_list[-1] += "1"
		cyc_smile = "".join(cyc_smile_list)
		benz_dict[cyc_smile] = name
	return benz_dict
	
def pyr_smile_gen(nsubst):
	'''Automate the generation of all substitution patterns in aromatic rings
	Input:
	- nsubst. Integer. Number of nitrogen atoms + additional substituted positions
	Output:
	- pyridine_dict. Dictionary mapping simplified SMILES to human-readable names, for the
	requested substituted pyridine'''
	# Init dict
	pyridine_dict = {}
	# Two situations: unsubstituted & substituted pyridines
	# 1. Non-substituted:
	if (nsubst == 1): 
		for ii in range(0,6):
			unsubst_list = ["[cH]" for x in range(1,7)]
			unsubst_list[ii] = "[n]"
			unsubst_list[0] += "1"
			unsubst_list[-1] += "1"
			unsubst_smile = "".join(unsubst_list)
			pyridine_dict[unsubst_smile] = "pyridine"
	# 2. Substitutions: use combinatorics
	else:
		# generate combinations
		string_vals = [str(x) for x in range(2,7)]
		# use permutations to distinguish all positions to set nitrogens
		combx = list(itertools.permutations(string_vals,r=nsubst-1))
		# and these are already sorted! Add the 1st item, always 1
		stringlist = []
		for entry in combx:
			# 1st item in comb. will be the nitrogen atom, rest substituents
			# remember that 1st pos. in the SMILES is always branching point
			Nat = ",N" + entry[0]
			other_at = "".join(entry[1:])
			stringlist.append("1" + other_at + Nat)
		# these are the possible codes: build SMILES
		for entry in stringlist:
			cpos,npos = entry.split(",")
			name = "pyridine-" + npos
			n_ndx = int(npos[1]) - 1
			values = [int(ii) for ii in cpos]
			cyc_smile_list = ["[c]" if x in values else "[cH]" for x in range(1,7)]
			# add the nitrogen
			cyc_smile_list[n_ndx] = "[n]"
			# mark start and end
			cyc_smile_list[0] += "1"
			cyc_smile_list[-1] += "1"
			cyc_smile = "".join(cyc_smile_list)
			pyridine_dict[cyc_smile] = name
	return pyridine_dict

# General cycle substitution detection
def locate_subst_in_cycle(in_string):
	'''Locate unsatisfied valences in cycle fragments to determine branch positioning
	in humanized names: uses inner dictionary with no. of valences of most common cycle-forming groups:
	C, N, O, S
	Input:
	- in_string. String, SMILES-like fragment corresponding to the isolated cyclic structure. Subst. pattern
	is determined depending on the no. of hydrogens in each cycle member.
	Output:
	- vacancies. List of lists with information about the vacancies along the cycle. Contains:
		+ Position of the current vacancy in the cycle (integer).
		+ Indices for the slice corresponding to the current vacancy (tuple of integers). 
		+ String for the current vacancy slice (string).
		+ Number of vacant positions in the current position (integer).'''
	# inner dict to check valences
	basic_valences_cycles = {
	"C":2,"CH":1,"CH2":0,
	"c":1,"cH":0,
	"N":1,"NH":0,
	"n":0,
	"O":0,"o":0,"S":0,"s":0
	}	
	ndx_list = first_level_locator(in_string,'bracket')
	vacants = []
	for ii,ndx in enumerate(ndx_list):
		a = ndx[0] 
		b = ndx[1] 
		# slice the entry
		entry = in_string[a+1:b]
		# compare with dict
		try:
			Nvacant = basic_valences_cycles[entry]
		except:
			Nvacant = 0
		# check for double bonds before or after the group: if these exist, remove a vacant
		before_group = in_string[a-1]
		after_group = in_string[b+1]
		if ("=" in before_group) or ("=" in after_group):
			Nvacant = Nvacant - 1
		# and append info for all entries with unsatisfied valences
		if (Nvacant != 0):
			vacants.append([ii+1,ndx,entry,Nvacant])
	return vacants

# Dictionary of cycle patterns. Combine manually-curated entries with
# auto-generated entries for subst. benzenes 

def cycle_dict_generation():
	'''Generation of a dictionary of common cycle patterns, via population functions
	Input:
	- None
	Output:
	- cycle_dict. Dictionary containing SMILES:names'''
	cycle_dict = {}
	benz_dictionaries = [benz_smile_gen(ii) for ii in range(2,6)]
	[cycle_dict.update(bdict) for bdict in benz_dictionaries]
	# also include unsubst. pyridines -> go from 1 to 6
	pyr_dictionaries = [pyr_smile_gen(ii) for ii in range(1,6)]
	[cycle_dict.update(pdict) for pdict in pyr_dictionaries]
	# Add entries for unsubstituted benzene (!)
	cycle_dict["[cH]1[cH][cH][cH][cH][cH]1"] = "C6H6"
	return cycle_dict


# summarize methylenes
def ch2_shortener(summ_string):
	'''For strings (simplified SMILES!) with consecutive methylene groups,
	summarize them in the form CH2CH2CH2... -> (CH2)N -> recursive action.
	Input:
	- summ_string. String, simplified SMILES.
	Output:
	- new_string. String, simplified SMILES with compact methylenes'''
	ch2_regex = r"(CH2){2,}"
	# match the first chain to be replaced: get length and replace
	match = re.search(ch2_regex,summ_string)
	if (match):
		orig_form = match.group()  
		nch2 = int(match.span()[1] - match.span()[0])/3
		new_form = "(CH2)%d" % nch2
		# make sure to only replace one group at a time
		new_string = re.sub(orig_form,new_form,summ_string,count=1)
		# recursive action
		new_string = ch2_shortener(new_string)
	else:
		# do the replacement
		new_string = summ_string
	return new_string

def cycle_humanizer(ndx,entry,cycle_dict):
	'''Apply humanization for 1-cyclic structures.
	Input:
	- ndx. List containing lists of lists wth the indices of the bracket-delimited cycle-closing groups.
	- entry. String, SMILES for the molecule under processing.
	- cycle_dict. Dictionary containing heuristics for cycle naming.
	Output:
	- simple_str. String, summarized name.
	'''
	# dictionary of disubst. benzenes
	di_benz = {
			"12-C6H4":"o-C6H4",
			"13-C6H4":"m-C6H4",
			"14-C6H4":"p-C6H4",
			"15-C6H4":"m-C6H4",
			"16-C6H4":"o-C6H4",
	}
	named_flag = False
	# Start processing
	start = ndx[0][0]
	end = ndx[1][1]
	frag_base = entry[0:start]
	# process the base fragment, removing brackets and asterisks
	frag_base_str = re.sub("\]","",re.sub("\[","",frag_base))
	frag_base_str = re.sub("\*","",frag_base_str)
	if (frag_base_str):
		frag_base_str += "-"
	# isolate by found indices & analyze
	cycle = entry[start:end]
	# smart parenthesis matching
	breakpoints = first_level_locator(cycle)
	# remove the found fragments (to have basic cycle) and isolate these in a list
	maincyc,branch = string_remover(cycle,breakpoints,True)
	maincyc_str = "".join(maincyc)
	# add the end fragments to the list of branches
	branch = branch + [entry[end:]]
	# Handle situations where one of the branches is a 1-cycle -> SMILES pre-processing already places substituents in a reasonable manner
	cyc_in_branch = [cycle_finder(br) for br in branch]
	# process only when there are cycle: recursive application!
	if (np.any(np.array(cyc_in_branch,dtype=object))):
		nw_branch = ["(%s)" % cycle_humanizer(bndx,br,cycle_dict) 
							if len(bndx) > 0 else br for bndx,br in zip(cyc_in_branch,branch)]
		branch = nw_branch
	# is there any cycle as a branch????
	# wherever this is not 0, replace branch name by cycle -> recursive action!
	branch_str = ",".join(branch)
	# remove brackets from branch
	branch_str = re.sub("\]","",re.sub("\[","",branch_str))
	# substituent location
	vacancies = locate_subst_in_cycle(maincyc_str)
	# base fragment is always first pos.
	# sanity check: no. of branches should match no. of positions to occupy
	hang_groups = [item[1:-1] for item in branch if item]
	hanging_pos_raw = [[ventry[0]]*ventry[3] for ventry in vacancies]
	# flatten
	hanging_pos = list(itertools.chain.from_iterable(hanging_pos_raw))
	# build prefix for the substitution pattern
	subst_string = "".join([str(pos) for pos in hanging_pos]) + "-"
	# remove 1st pos of string, which always indicates 1 (branching pos.)
	# subst_string = subst_string[1:]
	if (subst_string  == "-"):
		# exclude monosubstitution cases
		subst_string = ""
			
	for cyc_match,cyc_str in cycle_dict.items():
		if (maincyc_str == cyc_match):
			subst_cyc = "%s%s" % (subst_string,cyc_str)
			# additional layer of simplification: disubst benzenes
			if (subst_cyc in di_benz.keys()):
				subst_cyc = di_benz[subst_cyc]
			simple_str = "%s%s-%s" % (frag_base_str,subst_cyc,branch_str)
			named_flag = True
	# flag cases where the cycle was not recognized
	if (named_flag == False):
		Nitems = len(re.findall("\[",maincyc_str))
		# remove cycle flags and join
		id_gen = re.sub("\]1","]",maincyc_str)
		id_proc = "%scyc%d-" % (subst_string,Nitems) + (re.sub("\]","",re.sub("\[","",id_gen))).upper()
		# if not branched, generate a simple identifier
		if (not branch_str):
			# count entries
			simple_str = frag_base_str + id_proc
		else:
			# rebuild the branch string
			simple_str = frag_base_str + id_proc + "-" + branch_str
	return simple_str

def name_cleanup(name_in):
	'''Small formatting tweaks for humanized SMILES, to apply at the end of the naming
	protocol to have cleaner results.
	Input:
	- name_in. String, humanized name for a SMILES string
	Output:
	- name. String, re-formatted humanized name'''
	# clean -()- segments resulting from branched 1-cycles
	name = copy.copy(name_in)
	name = name.replace("-()-",",")
	# regiochemistry marks: /C=C/ and /C=C\ for trans- and cis- double bonds
	# remove, but might implement some recognition later
	name = name.replace("/","").replace("\\","")
	# stereochemistry: @ mark does not make sense now as there might have been reordering, just remove
	name = name.replace("@","")
	# remove double hyphens
	name = name.replace("--","-")
	# fix final hyphens and commas
	last_char = name[-1]
	if (last_char in [",","-"]):
		name = name[:-1]
	return name

def smile_humanizer(smile_db):	
	'''Apply the Human-SMILES protocol to a given list of strings. Steps:
	1. Prepare the SMILES, so it includes [*] group and explicit hydrogens, via re_smiler()
	2. Initial simplification of monodentate groups, via common_simplifier()
	3. Cycle processing: location via cycle_finder(), isolation of cycle fragment and comparison with library
	4. Methylene processing, via ch2_shortener()
	Input:
	- smile_db. List of SMILES to be processed.
	Output:
	- summarized_subset. List with all valid processed SMILES, containing:
		+ Index of the original SMILES (integer)
		+ Human-readable SMILES (string)
		+ Original SMILES (string)
	'''
	# 1. Reformatting, explicit H
	outs_subset_raw = re_smiler(smile_db)
	# 2. Basic monodentate group simplification
	outs_subset = [common_simplifier(sm) for sm in outs_subset_raw]
	# 3. Cycle processing
	summarized_subset = []
	skipped_strucs = []
	# Generation of the dictionary of cycles
	cycle_dict = cycle_dict_generation()
	for ie,entry in enumerate(outs_subset):
		# skip bicyclic structures (by now), including these which are ]12... where the atom closes more than one cycl
		bicyclo_flag = re.search("\](1)?[2-9]",entry)
		if (bicyclo_flag):
			skipped_strucs.append(entry)
			continue
		# Locate single cycles
		ndx = cycle_finder(entry)
		# Cases where the number of entries is ODD cannot be handled: discard
		if (len(ndx) % 2 != 0):
			skipped_strucs.append(entry)
			continue
		elif (len(ndx) > 0):
			simple_str = cycle_humanizer(ndx,entry,cycle_dict)
		else:
			# non-cyclic structures: just remove brackets for naming
			simple_str = re.sub("\]","",re.sub("\[","",entry))
			simple_str = re.sub("\*","",simple_str)
		# prepare the valid SMILES and wrap results
		orig_smile = smile_db[ie]
		# additional sanity check: the string MUST NOT BE EMPTY
		if (not simple_str):
				continue
        # wrap the methylenes
		simple_str_flat = ch2_shortener(simple_str)
		# fix final hyphens and commas
		simple_str_flat = name_cleanup(simple_str_flat)
		summarized_subset.append([ie,simple_str_flat,orig_smile])
	return summarized_subset

def name_policy(name_subset):
	'''
	From the [index, humanized SMILES, original SMILES] list resulting from smile_humanizer(),
	remove all entries that could not be properly auto-named, flagged as MISSING_
	Input:
	- name_subset. List of lists generated by smile_humanizer()
	Output:
	- valid_subset. List of lists, with all MISSING entries removed
	'''
	valid_subset = [entry for entry in name_subset if "MISSING" not in entry[1]]
	return valid_subset

def list_mol_generator(all_smiles):
	'''Basic generation of RDKit molecules from a list of SMILES
	Input:
	- all_smiles. List of SMILES
	Output:
	- mol_list. List of RDKit molecules'''
	# From a list of SMILES, read the corresponding molecules
	# first change [R] by [*] for more general support
	alt_smiles = [re.sub("\[R\]","*",smx) for smx in all_smiles]
	mol_list = [Chem.MolFromSmiles(smx) for smx in alt_smiles]
	return mol_list

def image_generator(summ_names,Nligs_page,mapname="ligmap_",with_names=True,with_ndx=True,mols_row=8,sub_size=(250,200)):
	'''Generation of grids of images from a list of human-readable SMILES (for naming)
	and the corresponding SMILES (for image generation).
	Input:
	- summ_names. Output from smile_humanizer, containing index/human-readable SMILES/original SMILES
	- Nligs_page. Integer, number of ligs per grid
	- mapname. String, prefix for image files.
	- with_names. Boolean, if True include human-readable SMILES in the image
	- with_ndx. Boolean, if True include structure index in the figure
	- mols_row. Integer, no. of molecules per row.
	- sub_size. Tuple of floats, size of individual figures.
	Output:
	- img. Grid image object from RDKit'''
	# when XYZ files are produced, we will already have lists of accepted molecules
	ct = 0
	Npages = ceil(len(summ_names)/Nligs_page)
	# From pre-processed list of names, take the original SMILES and re-read the molecule
	# Easier workflow: storing & using pre-generated molecules would require to pre-treat conformers
	passed_smiles = [item[2] for item in summ_names]
	mols = list_mol_generator(passed_smiles)
	
	for page in range(Npages):
		p0 = Nligs_page*page
		p1 = p0 + Nligs_page
		# read the corresponding entry
		mol_sel = mols[p0:p1]
		if (with_names and with_ndx):
			mol_indices = ["   ".join([str(item[0]),item[1]]) for item in summ_names[p0:p1]]
		elif (not with_names and with_ndx):
			mol_indices = [str(item[0]) for item in summ_names[p0:p1]]
		elif (with_names and not with_ndx):
			mol_indices = [item[1] for item in summ_names[p0:p1]]
		else:
			mol_indices = ["" for item in summ_names[p0:p1]]
		# Last version of RDKit changed image generation: try/except to keep compatibility between versions
		imgname = "%s%02d.png" % (mapname,page)
		try:
			# This works for RDKit 2020.09.3
			img = Draw.MolsToGridImage(mol_sel,molsPerRow=mols_row,legends=mol_indices,
											subImgSize=sub_size,maxMols=Nligs_page,returnPNG=True)
			with open(imgname,"wb") as fpng:
				fpng.write(img.data)
		except:
			# Fallback for previous versions
			img = Draw.MolsToGridImage(mol_sel,molsPerRow=mols_row,legends=mol_indices,
											subImgSize=sub_size,maxMols=Nligs_page)
			img.save(imgname)
	return img

