# Human-Readable SMILES: Translating Cheminformatics to Chemistry
Diego Garay-Ruiz, **2021** <br>
*Institute of Chemical Research of Catalonia* <br>
Prof. Carles Bo Research Group

## Introduction
HumanSMILES is a Python library designed to generate human-readable names for small molecules from their SMILES string representations. These names are inspired by the simplified molecular formulas and abbreviations employed in Organic Chemistry, and aim to be much easier to understand than SMILES or other molecular string representations.

The idea behind HumanSMILES is to be a *complement* to improve the understandability of the numerous computational protocols relying on SMILES, providing the user with a name that can be easily interpreted as the underlying molecule. In this way, it becomes possible to obtain more readable code and clearer results with minimal effort.

Currently, HumanSMILES does only support *acyclic*, *single-cycle* and *non-fused polycyclic* molecules: the general application to fused polycycles is limited due to the general problematic existing on the systematic naming of this kind of structures. Despite this limitation, tests on existing databases (see `examples/` folder for details) show a good performance on both precurated sets (`examples/subst_example`, with data from P. Ertl, *J. Cheminf*, **2020**) and general small molecule collections (`examples/GDB_example`, with data from the GDB-10 database: T. Fink, H. Bruggesser and J.L. Raymond, *Angew. Chem. Int. Ed*, **2005** and T.Fink and J.L. Raymond, *J. Chem. Inf. Model.*, **2007**).

More details on the algorithm and its scientific applications would be provided in a future paper [D. Garay-Ruiz and C. Bo, *manuscript under preparation*]. The basic outline of the program is as follows:
1. Generate explicit hydrogens from input SMILES (read and regenerate via RDKit)
2. Pass a basic dictionary to substitute common patterns.
3. Detect cyclic structures.
4. Process cycles:
    - Find cyclic fragment.
    - Isolate main cycle and branches, including positions.
    - Analyze the cycle to assign a common name or an automatic one.
5. Summarize long methylene chains.
6. Clean up the obtained name.

## Dependencies
- RDKit (>2020.03)
The library relies on RDKit (https://www.rdkit.org/) functionality for SMILES input and a first preprocessing step, and also for image generation.

## Installation
A basic installer from setuptools, `setup.py` is provided. Inside the main folder, run:

```
pip install -e .
```
to install the package in *editable* mode, so any change in the **.py** files from a custom fork or a new version of the code is immediately applied to the existing installation.

## Usage
Jupyter Notebooks are provided in the `examples/` folder for a reference on program setup for different molecule sets.
For translating a single SMILES at a time, the user should:
```python
import HumanSMILES as HS
smiles = "Clc1ccc(Cl)cc1"    # paradichlorobenzene
HS.smile_humanizer([smiles]) # must pass a list
```

Additionally, a demo web application (https://humansmiles-web.herokuapp.com/) is also available to do a quick testing of HumanSMILES capabilities, passing one SMILES at a time.